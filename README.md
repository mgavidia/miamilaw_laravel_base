# MiamiLaw Laravel Base Package

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Total Downloads][ico-downloads]][link-downloads]
[![StyleCI][ico-styleci]][link-styleci]

This package is the basis for MiamiLaw Laravel Projects. It provides:
 - Roles
 - Web templates
 - SSO to Azure AD depending on [Laravel Socialite Azure Active Directory Plugin][link-laravel-azure]  

## Installation

##Via Composer
>For local development, add the repository as a local path:
``` json
"repositories": [
    {
        "type": "path",
        "url": "/path/to/miamilaw_laravel_base"
    }
]
```
>Add to composer.json
``` bash
$ composer require miamilaw/base
```

>Post installation requirements

``` bash
$ php artisan make:auth
$ php artisan vendor:publish --provider="Metrogistics\AzureSocialite\ServiceProvider" --force
$ php artisan vendor:publish --provider="MiamiLaw\Base\BaseServiceProvider" --force
```

##Database
###Local development using Sqlite
```bash
$ touch database/local.sqlite
```
Edit .env
```bash
DB_CONNECTION=sqlite
DB_DATABASE=/path/to/database/local.sqlite
```  
>Or, setup a preferred database as per [the documentation](https://laravel.com/docs/5.8/database).

## Usage

###Include Service Providers
Add service provider to config/app.php
```php
'providers' => [
    // ...
    
    /*      
     * Package Service Providers...
     */
         
    MiamiLaw\Base\BaseServiceProvider::class,
    
    // ...
]
```

###Add roles to User
Add the trait Roles to *app/User.php*
```php
<?php
//...
use MiamiLaw\Base\Traits\Roles;

class User extends Authenticatable
{
    use Notifiable, Roles;
//...
}
```

###Attach a role to new users (Optional)
Add or replace the following in *app/Http/Controllers/Auth/RegisterController.php* 
```php
protected function create(array $data)
{
    $user = User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password']),
    ]);

    // Assign Role (guest for example)
    $user
        ->roles()
        ->attach(Role::where('name', 'guest')->first());

    return $user;
}
```

###Role Based Checks
Controller wide add the middleware to constructor along with auth
```php
public function __construct() {
        $this->middleware('auth');
        $this->middleware('checkadmin');
}
```
For specific actions
```php
public function myAction(Request $request){
    // Check for admin role
    $request->user()->authorizeRoles(['admin']);
    // ...
}
```
>In order for the middleware to work you may need to move 
*\Illuminate\Session\Middleware\StartSession::class* in *app/Http/Kernel.php* from the
application $middlewareGroups to the global $middleware

##Migrations
Run the database migrations
``` bash
$ php artisan migrate:fresh
```
>Note if using sqlite make sure to require doctrine/dbal

##Add Initial Admin User to *.env* for seeding
```bash
MIAMILAW_BASE_ADMIN_NAME="Last Name, First Name"
MIAMILAW_BASE_ADMIN_EMAIL=myuser@someplace.com
MIAMILAW_BASE_ADMIN_AZURE_ID=####
MIAMILAW_BASE_ADMIN_LOCAL_PASSWORD=####
```
>Make sure to clear the cache

Run the seed data to add basic roles and users
```bash
$ php artisan config:cache
$ php artisan db:seed --class=MiamiLaw\\Base\\Database\\Seeds\\DatabaseSeeder
```

If desirable, to aid in development add the following to *database/seeds/DatabaseSeeder.php*
```php
...
use MiamiLaw\Base\Database\Seeds\DatabaseSeeder as BaseSeeder;
...

    public function run()
    {
    	$this->call(BaseSeeder::class);
    }
...
```

##Setting the type of Authentication
Remove the Auth default Routes from *routes/web.php*
```php
// Auth::routes();
```  

In configuration file *config/miamilaw.php' you can set auth to use either *'local'* or *'azure'*.
Clear the cache
```bash
php artisan config:cache
```

###Using Local Authentication

Adding an admin user using the tinker tool (or see section Artisan Commands)
```bash
$ php artisan tinker
```
```php
Psy Shell v0.9.9 (PHP 7.2.17-0ubuntu0.18.04.1 — cli) by Justin Hileman
>>> $user = new User();
>>> $user->name = 'Last name, First Name';
>>> $user->email = 'lf@yourcompany.com';
>>> $user->password = Hash::make('password');
>>> $user->save();
>>> $role_admin = Role::where('name', 'admin')->first();
>>> $user->roles()->attach($role_admin);
>>> exit
```

###Configure Azure SSO Authentication 
Create a new application on Azure Active Directory. See the following article for more details on registering an application.
>[Quickstart: Register an application with the Microsoft identity platform](https://docs.microsoft.com/en-us/azure/active-directory/develop/quickstart-register-app)

After registering your application edit .env and add the client and secret keys
```bash
AZURE_AD_CLIENT_ID=XXXX
AZURE_AD_CLIENT_SECRET=XXXX
```
>[Click here][link-laravel-azure] for more information regarding the plugin

###Artisan commands
Run artisan to see the options
```bash
$ php artisan
...
make
  ...
  make:miamilaw-crud   Create CRUD for my miamilaw templates
  ...
...
user
  user:add             Modify a users attributes
  user:delete          Delete a user
  user:list            Get list of all available users
  user:modify          Modify a user's attributes
...
```
####Generating CRUD
An example of generating CRUD
```bash
$ php artisan make:miamilaw-crud Example
```
Will produce the following items and add an entry to *routes/web.php*
 - app/Example.php
 - app/Http/Controllers/ExampleController.php
 - database/migrations/####_##_##_######_create_examples_table.php
 - resources/views/admin/example/index.blade.php
 - resources/views/admin/example/create.blade.php
 - resources/views/admin/example/edit.blade.php

####User utilities
You can list, add, modify, and delete users directly by running one of the following commands
```bash
$ php artisan user:list
----------------------------
$ php artisan user:add
----------------------------
$ php artisan user:modify
----------------------------
$ php artisan user:delete
``` 

###Routes
Routes currently provided
```bash
+--------+-----------+-------------------------------------+--------------------+------------------------------------------------------------+-----------------+
| Domain | Method    | URI                                 | Name               | Action                                                     | Middleware      |
+--------+-----------+-------------------------------------+--------------------+------------------------------------------------------------+-----------------+
|        | POST      | admin/menu                          | menu.store         | MiamiLaw\Base\Http\Controllers\MenuController@store        | auth,checkadmin |
|        | GET|HEAD  | admin/menu                          | menu.index         | MiamiLaw\Base\Http\Controllers\MenuController@index        | auth,checkadmin |
|        | GET|HEAD  | admin/menu/create                   | menu.create        | MiamiLaw\Base\Http\Controllers\MenuController@create       | auth,checkadmin |
|        | DELETE    | admin/menu/{menu}                   | menu.destroy       | MiamiLaw\Base\Http\Controllers\MenuController@destroy      | auth,checkadmin |
|        | PUT|PATCH | admin/menu/{menu}                   | menu.update        | MiamiLaw\Base\Http\Controllers\MenuController@update       | auth,checkadmin |
|        | GET|HEAD  | admin/menu/{menu}/edit              | menu.edit          | MiamiLaw\Base\Http\Controllers\MenuController@edit         | auth,checkadmin |
|        | POST      | admin/menu/{menu}/items             | menu.items.store   | MiamiLaw\Base\Http\Controllers\MenuController@itemsStore   | auth,checkadmin |
|        | GET|HEAD  | admin/menu/{menu}/items             | menu.items.index   | MiamiLaw\Base\Http\Controllers\MenuController@itemsIndex   | auth,checkadmin |
|        | GET|HEAD  | admin/menu/{menu}/items/create      | menu.items.create  | MiamiLaw\Base\Http\Controllers\MenuController@itemsCreate  | auth,checkadmin |
|        | PUT|PATCH | admin/menu/{menu}/items/{item}      | menu.items.update  | MiamiLaw\Base\Http\Controllers\MenuController@itemsUpdate  | auth,checkadmin |
|        | DELETE    | admin/menu/{menu}/items/{item}      | menu.items.destroy | MiamiLaw\Base\Http\Controllers\MenuController@itemsDestroy | auth,checkadmin |
|        | GET|HEAD  | admin/menu/{menu}/items/{item}/edit | menu.items.edit    | MiamiLaw\Base\Http\Controllers\MenuController@itemsEdit    | auth,checkadmin |
|        | POST      | admin/user                          | user.store         | MiamiLaw\Base\Http\Controllers\UserController@store        | auth            |
|        | GET|HEAD  | admin/user                          | user.index         | MiamiLaw\Base\Http\Controllers\UserController@index        | auth            |
|        | GET|HEAD  | admin/user/create                   | user.create        | MiamiLaw\Base\Http\Controllers\UserController@create       | auth            |
|        | DELETE    | admin/user/{user}                   | user.destroy       | MiamiLaw\Base\Http\Controllers\UserController@destroy      | auth            |
|        | PUT|PATCH | admin/user/{user}                   | user.update        | MiamiLaw\Base\Http\Controllers\UserController@update       | auth            |
|        | GET|HEAD  | admin/user/{user}                   | user.show          | MiamiLaw\Base\Http\Controllers\UserController@show         | auth            |
|        | GET|HEAD  | admin/user/{user}/edit              | user.edit          | MiamiLaw\Base\Http\Controllers\UserController@edit         | auth            |
|        | GET|HEAD  | login/microsoft                     |                    | Closure                                                    |                 |
|        | GET|HEAD  | login/microsoft/callback            |                    | Closure                                                    |                 |
|        | GET|HEAD  | logout                              |                    | Closure                                                    | web             |
|        | GET|HEAD  | user/profile                        | user.profile       | MiamiLaw\Base\Http\Controllers\UserController@profile      | auth            |
+--------+-----------+-------------------------------------+--------------------+------------------------------------------------------------+-----------------+
```

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email m.gavidia@miami.edu instead of using the issue tracker.

## Credits

- [mgavidia][link-author]
- [All Contributors][link-contributors]

## License

license. Please see the [license file](https://bitbucket.org/mgavidia/miamilaw_laravel_base/src/master/LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/miamilaw/base.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/miamilaw/base.svg?style=flat-square
[ico-styleci]: https://bitbucket.styleci.io/repos/-4141313558888160418/shield

[link-packagist]: https://packagist.org/packages/miamilaw/base
[link-downloads]: https://packagist.org/packages/miamilaw/base
[link-styleci]: https://bitbucket.styleci.io/repos/-4141313558888160418
[link-author]: https://bitbucket.org/mgavidia
[link-contributors]: ../../contributors

[link-laravel-azure]: https://github.com/metrogistics/laravel-azure-ad-oauth