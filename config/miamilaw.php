<?php

return [
    // Use azure or local
    'auth' => 'local',
    'admin_seed' => [
        'name' => env('MIAMILAW_BASE_ADMIN_NAME', ''),
        'email' => env('MIAMILAW_BASE_ADMIN_EMAIL', ''),
        'azure_id' => env('MIAMILAW_BASE_ADMIN_AZURE_ID', ''),
        'local_password' => env('MIAMILAW_BASE_ADMIN_LOCAL_PASSWORD', ''),
    ],
];
