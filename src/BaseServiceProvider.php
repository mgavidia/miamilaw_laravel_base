<?php

namespace MiamiLaw\Base;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Metrogistics\AzureSocialite\ServiceProvider as AzureSSO;

// use Metrogistics\AzureSocialite\AzureUser;

class BaseServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'miamilaw');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'miamilaw');
        $this->loadMigrationsFrom(__DIR__.'/Database/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }

        // Middleware aliases
        $this->app['router']->aliasMiddleware('checkadmin', Http\Middleware\CheckAdmin::class);

        // Azure SSO
        $this->app->register(AzureSSO::class);
        $this->app->register(BladeServiceProvider::class);
        $loader = AliasLoader::getInstance();
        $loader->alias('Metrogistics\AzureSocialite\AzureUser', 'AzureUser');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/miamilaw.php', 'miamilaw');

        // Register the service the package provides.
        $this->app->singleton('miamilaw', function ($app) {
            return new Base;
        });

        // Handle events
        $this->app->register(EventServiceProvider::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['miamilaw'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/miamilaw.php' => config_path('miamilaw.php'),
        ], 'miamilaw');

        // Publishing the views.
        $this->publishes([
            __DIR__.'/resources/views/home.blade.php' => base_path('resources/views/home.blade.php'),
            __DIR__.'/resources/views/auth' => base_path('resources/views/auth'),
        ], 'miamilaw');

        // Publishing assets.
        $this->publishes([
            __DIR__.'/resources/dist' => public_path('vendor/miamilaw'),
        ], 'public');

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/miamilaw'),
        ], 'miamilaw.base.views');*/

        // Registering package commands.
        $this->commands([
            Console\Commands\CrudGenerator::class,
            Console\Commands\UserAdd::class,
            Console\Commands\UserDelete::class,
            Console\Commands\UserList::class,
            Console\Commands\UserModifier::class,
        ]);
    }
}
