<?php

namespace MiamiLaw\Base;

use MiamiLaw\Base\Models\Menu;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Check if Role is Admin
        Blade::if('isadmin', function () {
            try {
                DB::connection()->getPdo();

                return Auth::check() && Auth::user()->hasRole('admin');
            } catch (\Exception $ex) {
            }

            return false;
        });

        Blade::if('notadmin', function () {
            try {
                DB::connection()->getPdo();

                return ! Auth::check() || (Auth::check() && ! Auth::user()->hasRole('admin'));
            } catch (\Exception $ex) {
            }

            return true;
        });

        // Check Role
        Blade::if('roleis', function ($role) {
            try {
                DB::connection()->getPdo();

                return Auth::check() && Auth::user()->hasRole($role);
            } catch (\Exception $ex) {
            }

            return false;
        });

        // Check if Route is Valid
        Blade::if('validroute', function ($route) {
            $routes = Route::getRoutes();
            $request = Request::create($route);
            try {
                $routes->match($request);
                // route exists
                return true;
            } catch (NotFoundHttpException $e) {
                // route doesn't exist
                return false;
            }
        });

        // Check if route is current route
        Blade::if('currentroute', function ($route) {
            $path = Request::path();

            return $path === $route;
        });

        // Check if auth type is either local or azure
        Blade::if('authtype', function ($type) {
            return config('miamilaw.auth') === $type;
        });

        // Template Menu
        Blade::directive('rendermenu', function ($menu_name) {
            return Blade::compileString(
                '@php try { @endphp'.
                '@php if (Illuminate\Support\Facades\Schema::hasTable("menus")) : @endphp'.
                '@php $items = MiamiLaw\Base\Models\Menu::where("name", "'.$menu_name.'")->first()->items; @endphp'.
                '@php foreach($items as $item) : @endphp'.
                '@php $active = \'/\' . Request::path() === $item->route ? "active" : ""; @endphp'.
                '@php echo \'<a class="\' . $active . \' item" href="\' . $item->route . \'">\' . $item->name . \'</a>\'; @endphp'.
                '@php endforeach; @endphp'.
                '@php endif; @endphp'.
                '@php } catch (\Exception $ex) { } @endphp'
            );
        });
    }
}
