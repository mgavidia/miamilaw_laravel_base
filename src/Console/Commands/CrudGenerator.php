<?php

namespace MiamiLaw\Base\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;

class CrudGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:miamilaw-crud {name : Class (singular) for example Page}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create CRUD for my miamilaw templates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $name = $this->argument('name');

        $this->controller($name);
        $this->model($name);
        $this->views($name);

        $resource = 'Route::resource(\'admin/'.strtolower($name)."', '{$name}Controller')->except(['show']);";
        // Check if already in routes/web.php
        if (strpos(file_get_contents(base_path('routes/web.php')), $resource) === false) {
            File::append(base_path('routes/web.php'), $resource);
        }
    }

    protected function getStub($type)
    {
        return file_get_contents(__DIR__."/../../resources/stubs/$type.stub");
    }

    protected function model($name)
    {
        $file = app_path("{$name}.php");
        if (! file_exists($file) || $this->confirm("$file model already exists. Overwrite?")) {
            $modelTemplate = str_replace(
                ['%modelName%'],
                [$name],
                $this->getStub('Model')
            );

            file_put_contents($file, $modelTemplate);
        }

        $migration_dir = base_path('database/migrations');
        $plural = strtolower(str_plural($name));
        if (empty(glob("$migration_dir/*create_{$plural}_table*.*")) || $this->confirm('Migration already exists. Do you wish to generate anyways?')) {
            // Use artisan to create the migration for the model
            Artisan::call('make:migration', ['name' => "create_{$plural}_table"]);
        }
    }

    protected function controller($name)
    {
        $file = app_path("Http/Controllers/{$name}Controller.php");
        if (! file_exists($file) || $this->confirm("$file controller already exists. Overwrite?")) {
            $controllerTemplate = str_replace(
                [
                    '%modelName%',
                    '%modelNamePluralLowerCase%',
                    '%modelNameSingularLowerCase%',
                ],
                [
                    $name,
                    strtolower(str_plural($name)),
                    strtolower($name),
                ],
                $this->getStub('Controller')
            );

            file_put_contents($file, $controllerTemplate);
        }
    }

    protected function getViewTemplate($template_name, $name)
    {
        return str_replace(
            [
                '%modelName%',
                '%modelNamePluralLowerCase%',
                '%modelNameSingularLowerCase%',
            ],
            [
                $name,
                strtolower(str_plural($name)),
                strtolower($name),
            ],
            $this->getStub("views/$template_name")
        );
    }

    protected function views($name)
    {
        $dir = strtolower($name);
        $path = base_path("resources/views/admin/$dir");
        if (! is_dir($path)) {
            mkdir($path, 0755, true);
        }

        if (! file_exists("$path/index.blade.php") || $this->confirm("$path/index.blade.php already exists. Overwrite?")) {
            file_put_contents("$path/index.blade.php", $this->getViewTemplate('index.blade', $name));
        }
        if (! file_exists("$path/create.blade.php") || $this->confirm("$path/create.blade.php already exists. Overwrite?")) {
            file_put_contents("$path/create.blade.php", $this->getViewTemplate('create.blade', $name));
        }
        if (! file_exists("$path/edit.blade.php") || $this->confirm("$path/edit.blade.php already exists. Overwrite?")) {
            file_put_contents("$path/edit.blade.php", $this->getViewTemplate('edit.blade', $name));
        }
    }
}
