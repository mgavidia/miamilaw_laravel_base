<?php

namespace MiamiLaw\Base\Console\Commands;

use App\User;
use MiamiLaw\Base\Models\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class UserAdd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modify a users attributes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->ask('What is the email of the user?');

        if (User::where('email', $email)->first()) {
            $this->error('User already exists. Use user:update instead. Aborting...');

            return -1;
        }

        $updatable = [
            'email' => $email,
        ];

        if ($name = $this->ask('What is the name of the user?')) {
            $updatable['name'] = $name;
        }
        if ($password = $this->secret('Set the password (default [""])', null)) {
            $updatable['password'] = Hash::make($password);
        }
        if ($azure_id = $this->ask('Set the Azure ID (default [""])', null)) {
            $updatable['azure_id'] = $azure_id;
        }

        // Update user values
        if ($updatable) {
            $user = User::create($updatable);
            $this->comment("Created new user `{$user->email}`.");
            if ($this->confirm('Do you want to specify the roles for this user?')) {
                $roles = [];
                foreach (explode(',', $this->ask('Input the roles as a comma separated list like `guest,admin`:', '')) as $role) {
                    if ($add = Role::where('name', $role)->first()) {
                        $roles[$role] = $add->id;
                    } else {
                        $this->comment("Could not find role `$role`. Skipping.");
                    }
                }
                if ($values = array_filter($roles)) {
                    $user->roles()->sync($values);
                    $this->comment('User roles updated.');
                }
            } else {
                $user->roles()->attach(Role::where('name', 'guest')->first());
                $this->comment('Added guest role by default.');
            }
        }

        $this->info('Done.');
    }
}
