<?php

namespace MiamiLaw\Base\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class UserList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get list of all available users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $headers = ['Name', 'Email', 'Azure ID', 'Roles', 'Created Date'];

        $users = User::all();

        $list = [];
        foreach ($users as $user) {
            $roles = $user->roles()->where('user_id', $user->id)->pluck('roles.name')->toArray();
            $array = [
                $user->name,
                $user->email,
                $user->azure_id,
                implode(',', $roles),
                $user->created_at,
            ];
            $list[] = $array;
        }

        $this->table($headers, $list);

        return 0;
    }
}
