<?php

namespace MiamiLaw\Base\Console\Commands;

use App\User;
use MiamiLaw\Base\Models\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class UserModifier extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:modify 
                            {email : For existing user}
                            {--R|roles= : Set user roles by name for example: guest,admin (overrides existing)}
                            {--P|promote : Promotes user to Administrator}
                            {--D|demote : Demotes user from Administrator}
                            {--N|name= : Update the name of the user}
                            {--A|azure_id= : Set the Azure ID}
                            {--password : Set the password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modify a user\'s attributes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');

        $user = User::where('email', $email)->first();
        if (! $user) {
            $this->error('User does not exist. Aborting...');

            return -1;
        } elseif (! array_filter($this->options())) {
            $this->info('No modifiers added, nothing to do.');

            return 0;
        }

        $updatable = [];

        if ($this->option('name')) {
            $updatable['name'] = $this->option('name');
        }
        if ($this->option('password')) {
            $password = $this->secret('Enter new password:');
            $updatable['password'] = Hash::make($password);
        }
        if ($this->option('azure_id')) {
            $updatable['azure_id'] = $this->option('azure_id');
        }

        // Update user values
        if ($updatable) {
            $user->update($updatable);
            $this->comment('Updated user values ['.implode(',', array_keys($updatable)).'].');
        }

        // Check role updates
        if ($this->option('demote')) {
            $user->roles()->detach(Role::where('name', 'admin')->first());
            $this->comment('Demoted user from Administrator.');
        }
        if ($this->option('promote') && ! $this->option('roles')) {
            // Promote the user to admin
            $user->roles()->syncWithoutDetaching([Role::where('name', 'admin')->first()->id]);
            $this->comment('Promoted user to Administrator.');
        } elseif ($this->option('roles')) {
            $roles = [];
            foreach (explode(',', $this->option('roles')) as $role) {
                if ($add = Role::where('name', $role)->first()) {
                    $roles[$role] = $add->id;
                } else {
                    $this->comment("Could not find role `$role`. Skipping.");
                }
            }
            if ($this->option('promote')) {
                if (! array_key_exists('admin', $roles)) {
                    $roles['admin'] = Role::where('name', 'admin')->first()->id;
                }
            }
            $user->roles()->sync(array_filter($roles));
            $this->comment('User roles updated.');
        }

        $this->info('Done.');
    }
}
