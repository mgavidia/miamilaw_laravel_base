<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('azure_id', 36)->nullable();
            $table->string('password')->nullable()->change();
            // Login information
            $table->datetime('last_login_at')->nullable();
            $table->string('last_login_ip')->nullable();
        });

        // Avoid annoying error modifying a table in sqlite with a nullable column
        /*Schema::table('users', function (Blueprint $table) {
            //
            $table->string('azure_id', 36)->nullable(false)->change();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('azure_id');
            $table->string('password')->nullable(false)->change();
            $table->dropColumn('last_login_at');
            $table->dropColumn('last_login_ip');
        });
    }
}
