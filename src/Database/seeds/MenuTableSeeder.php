<?php

namespace MiamiLaw\Base\Database\Seeds;

use MiamiLaw\Base\Models\Menu;
use Illuminate\Database\Seeder;
use MiamiLaw\Base\Models\MenuItem;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin menu
        $admin = $this->createMenu('admin-menu', 'Administration Quick Menu');
        $this->addMenuItem($admin, 'Configure Users', '/admin/user');
        $this->addMenuItem($admin, 'Configure Menus', '/admin/menu');

        // User Admin Menu
        $user = $this->createMenu('user-menu', 'User Administration Menu');
        // $this->addMenuItem($user, 'Profile', '/home');

        // Main Menu
        $main = $this->createMenu('main-menu', 'Main Menu');
        $this->addMenuItem($main, 'Home', '/');
    }

    private function createMenu($name, $description)
    {
        $menu = new Menu();
        $menu->name = $name;
        $menu->description = $description;
        $menu->permanent = 1;
        $menu->save();

        return $menu;
    }

    private function addMenuItem(&$menu, $name, $route)
    {
        static $count = 0;
        $item = new MenuItem();
        $item->name = $name;
        $item->route = $route;
        $item->weight = $count;
        $count = $count + 1;
        // $item->menu()->attach($menu);
        $menu->items()->save($item);
    }
}
