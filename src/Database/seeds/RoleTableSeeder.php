<?php

namespace MiamiLaw\Base\Database\Seeds;

use MiamiLaw\Base\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // admins
        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->description = 'Website Administrator';
        $role_admin->save();

        // Guests
        $role_guest = new Role();
        $role_guest->name = 'guest';
        $role_guest->description = 'Website Guest';
        $role_guest->save();
    }
}
