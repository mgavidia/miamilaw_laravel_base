<?php

namespace MiamiLaw\Base\Database\Seeds;

use App\User;
use MiamiLaw\Base\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create initial admin user
        if ((config('miamilaw.admin_seed.name') && config('miamilaw.admin_seed.email')) &&
            (config('miamilaw.admin_seed.local_password') || config('miamilaw.admin_seed.azure_id'))
        ) {
            $role_admin = Role::where('name', 'admin')->first();

            $user = new User();
            $user->name = config('miamilaw.admin_seed.name');
            $user->email = config('miamilaw.admin_seed.email');
            if (config('miamilaw.admin_seed.azure_id')) {
                $user->azure_id = config('miamilaw.admin_seed.azure_id');
            }
            if (config('miamilaw.admin_seed.local_password')) {
                $user->password = Hash::make(config('miamilaw.admin_seed.local_password'));
            }
            $user->save();
            $user->roles()->attach($role_admin);
        }
    }
}
