<?php

namespace MiamiLaw\Base;

use Illuminate\Auth\Events\Login;
use MiamiLaw\Base\Listeners\UserLogin;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Login::class => [
            UserLogin::class,
        ],
    ];

    public function boot()
    {
        parent::boot();
    }
}
