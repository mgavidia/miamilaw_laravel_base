<?php
/**
 * Created by PhpStorm.
 * User: mgavidia
 * Date: 5/24/19
 * Time: 11:52 AM.
 */

namespace MiamiLaw\Base\Http\Controllers;

use Illuminate\Http\Request;
use MiamiLaw\Base\Models\Menu;
use MiamiLaw\Base\Models\MenuItem;

class MenuController extends Controller
{
    /**
     * MenuController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkadmin');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::all();

        return view('miamilaw::admin.menu.index', compact('menus'));
    }

    public function create()
    {
        $title = 'Create new Menu';

        return view('miamilaw::admin.menu.create', compact('title'))
            ->withErrors([]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        Menu::create($request->all());

        return redirect()->route('menu.index')
            ->with('success', 'Menu successfully created.');
    }

    public function edit($id)
    {
        $menu = Menu::where('id', $id)->first();

        if (! $menu || $menu->permanent === '1') {
            abort(401, 'Invalid request.');
        }

        $title = 'Edit Menu';

        return view('miamilaw::admin.menu.edit', compact('menu', 'title'))
            ->withErrors([]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $menu = Menu::where('id', $id)->first();

        if (! $menu) {
            abort(401, 'Invalid request.');
        }

        $menu->update($request->all());

        return redirect()->route('menu.index')
                         ->with('success', 'Menu successfully updated.');
    }

    public function destroy($id)
    {
        $menu = Menu::where('id', $id)->first();

        if (! $menu) {
            abort(401, 'Invalid request.');
        }

        $items_deleted = $menu->items()->delete();

        $name = $menu->name;

        $menu->delete();

        return redirect()->route('menu.index')
                         ->with('success', "Successfully deleted $name menu and $items_deleted items.");
    }

    public function itemsIndex($menu_id)
    {
        $menu = Menu::where('id', $menu_id)->first();

        if (! $menu) {
            abort(401, 'Invalid request.');
        }

        return view('miamilaw::admin.menu.item.index', compact('menu'));
    }

    public function itemsCreate($menu_id)
    {
        $menu = Menu::where('id', $menu_id)->first();

        if (! $menu) {
            abort(401, 'Invalid request.');
        }

        return view('miamilaw::admin.menu.item.create', compact('menu'))
            ->withErrors([]);
    }

    public function itemsStore(Request $request)
    {
        $validatedData = $request->validate([
            'menu_id' => 'required',
            'name' => 'required',
            'route' => 'required',
            'weight' => 'required',
        ]);

        /*$item = MenuItem::create($request->all());*/

        $menu = Menu::where('id', $request->get('menu_id'))->first();

        $menu->items()->save(new MenuItem($request->all()));

        return redirect()->route('menu.items.index', $request->get('menu_id'))
                         ->with('success', 'Menu Item successfully created.');
    }

    public function itemsEdit($menu_id, $id)
    {
        $item = MenuItem::where('id', $id)->first();

        if (! $item) {
            abort(401, 'Invalid request.');
        }

        return view('miamilaw::admin.menu.item.edit', compact('menu_id', 'item'))
            ->withErrors([]);
    }

    public function itemsUpdate(Request $request, $menu_id, $id)
    {
        $request->validate([
            'menu_id' => 'required',
            'name' => 'required',
            'route' => 'required',
            'weight' => 'required',
        ]);

        $item = MenuItem::where('id', $id)->first();

        if (! $item) {
            abort(401, 'Invalid request.');
        }

        $item->update($request->all());

        return redirect()->route('menu.items.index', $item->menu_id)
                         ->with('success', 'Menu item successfully updated.');
    }

    public function itemsDestroy($menu_id, $id)
    {
        $menu = Menu::where('id', $menu_id)->first();

        $menu->items()->where('id', $id)->delete();

        return redirect()->route('menu.items.index', $menu_id)
                         ->with('success', 'Menu item successfully deleted.');
    }
}
