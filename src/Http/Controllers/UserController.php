<?php

namespace MiamiLaw\Base\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use MiamiLaw\Base\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check() && ! Auth::user()->hasRole('admin')) {
            return redirect()->route('user.show', Auth::user()->id);
        }

        $users = User::all();

        return view('miamilaw::admin.user.index', compact('users'));
    }

    public function create()
    {
        Auth::user()->authorizeRoles(['admin']);
        $roles = Role::all();

        return view('miamilaw::admin.user.create', compact('roles'))
            ->withErrors([]);
    }

    public function store(Request $request)
    {
        Auth::user()->authorizeRoles(['admin']);
        $validatedData = $request->validate([
            'name',
            'email',
        ]);

        $defaults = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
        ];

        if ($request->get('azure_id') !== null) {
            $defaults['azure_id'] = $request->get('azure_id');
        }

        if ($request->get('password') !== null) {
            $defaults['password'] = HASH::make($request->get('password'));
        }

        $user = User::create($defaults);

        $user->roles()->attach($request->get('roles'));

        return redirect()->route('user.index')
            ->with('success', "User {$user->sname} was successfully created.");
    }

    public function edit($id)
    {
        if (Auth::user()->id !== intval($id) && ! Auth::user()->hasRole('admin')) {
            abort(401, 'This action is unauthorized.');
        }

        $user = User::where('id', $id)->first();

        $roles = Role::all();

        if (! $user) {
            abort(401, 'Invalid request.');
        }

        return view('miamilaw::admin.user.edit', compact('user', 'roles'))
            ->withErrors([]);
    }

    public function show($id)
    {
        if (Auth::user()->id !== intval($id) && ! Auth::user()->hasRole('admin')) {
            abort(401, 'This action is unauthorized.');
        }

        $user = User::where('id', $id)->first();
        $roles = Role::all();

        if (! $user) {
            abort(401, 'Invalid request.');
        }

        if (Auth::user()->hasRole('admin') || Auth::user()->id === $user->id) {
            return view('miamilaw::admin.user.show', compact('user', 'roles'))
                ->withErrors([]);
        }
        abort(401, 'Invalid request.');
    }

    public function profile()
    {
        return $this->show(Auth::user()->id);
    }

    public function update(Request $request, $id)
    {
        if (Auth::user()->id !== intval($id) && ! Auth::user()->hasRole('admin')) {
            abort(401, 'This action is unauthorized.');
        }

        $user = User::where('id', $id)->first();

        if (! $user) {
            abort(401, 'Invalid request.');
        }

        $validatedData = $request->validate([
            'name',
            'email',
        ]);

        $defaults = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
        ];

        if ($request->get('azure_id') !== null) {
            $defaults['azure_id'] = $request->get('azure_id');
        }

        if ($request->get('password') !== null) {
            $defaults['password'] = HASH::make($request->get('password'));
        }

        $user->update($defaults);

        if (Auth::user()->hasRole('admin')) {
            $user->roles()->sync($request->get('roles'));
        }

        return redirect()->route('user.index')
            ->with('success', "User {$user->name} successfully updated.");
    }

    public function destroy($id)
    {
        Auth::user()->authorizeRoles('admin');

        $user = User::where('id', $id)->first();

        if (! $user) {
            abort(401, 'Invalid request.');
        }

        if (Auth::user()->id === intval($id)) {
            return redirect()->route('user.index')
                ->with('warn', 'Cannot delete self.');
        } elseif (User::all()->count() === 1) {
            return redirect()->route('user.index')
                ->with('warn', 'Cannot delete only user.');
        }

        $user->roles()->detach();
        $user->delete();

        return redirect()->route('user.index')
            ->with('success', 'Successfully deleted User.');
    }
}
