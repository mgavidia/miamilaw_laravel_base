<?php

namespace MiamiLaw\Base\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::check() || (Auth::check() && ! Auth::user()->hasRole('admin'))) {
            abort(401, 'This action is unauthorized.');
        }

        return $next($request);
    }
}
