<?php

namespace MiamiLaw\Base\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //

    protected $fillable = [
        'name',
        'description',
        'permanent',
    ];

    public function items()
    {
        return $this->hasMany(MenuItem::class)->orderBy('weight', 'asc');
    }
}
