<?php

namespace MiamiLaw\Base\Models;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    //

    protected $fillable = [
        'menu_id',
        'name',
        'route',
        'weight',
    ];

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
}
