<?php

namespace MiamiLaw\Base\Traits;

use MiamiLaw\Base\Models\Role;

trait Roles
{
    /**
     * This method is called upon instantiation of the Eloquent Model.
     * It adds the login fields to the "$fillable" array of the model.
     *
     * @return void
     */
    public function initializeRoles()
    {
        $this->fillable[] = 'last_login_at';
        $this->fillable[] = 'last_login_ip';
    }

    /**
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @param string|array $roles
     *
     * @return bool
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) ||
                abort(401, 'This action is unauthorized.');
        }

        return $this->hasRole($roles) ||
            abort(401, 'This action is unauthorized.');
    }

    /**
     * Check multiple roles.
     *
     * @param array $roles
     *
     * @return bool
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * Check one role.
     *
     * @param string $role
     *
     * @return bool
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }
}
