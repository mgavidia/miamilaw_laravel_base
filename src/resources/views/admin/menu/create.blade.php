@extends('miamilaw::layouts.miamilaw')

@section('subject')
    {{ $title }}
@endsection

@section('content')
    @component('miamilaw::components.form.form', [
                'action' => [
                    'route' => 'menu.store',
                    'parameters' => []
                ],
                'cancel' => [
                    'route' => 'menu.index',
                    'parameters' => []
                ],
                ])
            @component('miamilaw::components.form.input', [
                        'name' => 'name',
                        'icon' => 'user',
                        'placeholder' => 'Name',
                        'required' => true,
                        'autofocus' => true,
                        'errors' => $errors,
                        ])
            @endcomponent
            @component('miamilaw::components.form.input', [
                        'name' => 'description',
                        'icon' => 'info',
                        'placeholder' => 'Description',
                        'required' => true,
                        'errors' => $errors,
                        ])
            @endcomponent
            @component('miamilaw::components.form.checkbox', [
                        'name' => 'permanent',
                        'description' => 'Is permanent?',
                        'disabled' => true,
                        'errors' => $errors,
                        ])
            @endcomponent
    @endcomponent
@endsection