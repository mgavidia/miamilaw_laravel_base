@extends('miamilaw::layouts.miamilaw')

@section('subject')
    {{ $title }}
@endsection

@section('content')
    @component('miamilaw::components.form.form', [
                'action' => [
                    'route' => 'menu.update',
                    'parameters' => [
                        'id' => $menu->id
                    ]
                ],
                'cancel' => [
                    'route' => 'menu.index',
                    'parameters' => []
                ],
                'method' => 'PUT'
                ])
        @component('miamilaw::components.form.input', [
                    'name' => 'name',
                    'icon' => 'user',
                    'placeholder' => 'Name',
                    'required' => true,
                    'autofocus' => true,
                    'value' => $menu->name,
                    'errors' => $errors,
                    ])
        @endcomponent
        @component('miamilaw::components.form.input', [
                    'name' => 'description',
                    'icon' => 'info',
                    'placeholder' => 'Description',
                    'required' => true,
                    'value' => $menu->description,
                    'errors' => $errors,
                    ])
        @endcomponent
        @component('miamilaw::components.form.checkbox', [
                    'name' => 'permanent',
                    'description' => 'Is permanent?',
                    'checked' => $menu->permanent,
                    'disabled' => true,
                    'errors' => $errors,
                    ])
        @endcomponent
    @endcomponent
@endsection