@extends('miamilaw::layouts.miamilaw')

@section('subject')
    Menu Configuration
@endsection

@section('content')
    @if (session()->get('success'))
        <div class="ui positive message">
            <div class="header">{{ session()->get('success') }}</div>
        </div>
    @endif
    <a href="{{ route('menu.create') }}" class="positive ui fluid large button">
        <i class="icon plus square outline"></i>
        Create new
    </a>
    <table class="ui celled striped table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Permanent</th>
                <th>Total Links</th>
                <th>Functions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($menus as $menu)
                <tr>
                    <td data-label="Name">{{ $menu->name }}</td>
                    <td data-label="Descriptione">{{ $menu->description }}</td>
                    <td data-label="Permanent">{{ $menu->permanent === '1' ? 'Yes' : 'No'}}</td>
                    <td data-label="Total Links">{{ $menu->items()->count() }}</td>
                    <td data-label="Functions">
                        @if ($menu->permanent === '1')
                            <div class="ui left floated buttons">
                                <div class="ui green disabled button">Edit</div>
                                <a href="{{ route('menu.items.index', $menu->id) }}" class="ui blue button">Edit Links</a>
                            </div>
                            <div class="ui right floated red disabled button">Delete</div>
                        @else
                            <form action="{{ route('menu.destroy', $menu)}}" method="post" onsubmit="return confirm('Are you sure you want to Delete the Menu?');">
                                @csrf
                                @method('DELETE')
                                <div class="ui left floated buttons">
                                    <a href="{{ route('menu.edit',$menu->id) }}" class="ui green button">Edit</a>
                                    <a href="{{ route('menu.items.index', $menu->id) }}" class="ui blue button">Edit Links</a>
                                </div>
                                <button class="ui right floated red button" type="submit">Delete</button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection