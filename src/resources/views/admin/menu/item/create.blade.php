@extends('miamilaw::layouts.miamilaw')

@section('subject')
    Create new Item for {{ $menu->description }}
@endsection

@section('content')
    @component('miamilaw::components.form.form', [
                'action' => [
                    'route' => 'menu.items.store',
                    'parameters' => [
                        'menu_id' => $menu->id
                    ]
                ],
                'cancel' => [
                    'route' => 'menu.items.index',
                    'parameters' => [
                        'menu_id' => $menu->id
                    ]
                ],
                ])
        @component('miamilaw::components.form.hidden', [
                    'name' => 'menu_id',
                    'value' => $menu->id
                    ])
        @endcomponent
        @component('miamilaw::components.form.input', [
                    'name' => 'name',
                    'icon' => 'square',
                    'placeholder' => 'Name',
                    'required' => true,
                    'autofocus' => true,
                    'errors' => $errors,
                    ])
        @endcomponent
        @component('miamilaw::components.form.input', [
                    'name' => 'route',
                    'icon' => 'linkify',
                    'placeholder' => 'URL Route',
                    'required' => true,
                    'errors' => $errors,
                    ])
        @endcomponent
        @component('miamilaw::components.form.select', [
                    'name' => 'weight',
                    'description' => 'Select Weight',
                    'select' => [
                        'type' => 'numerical',
                        'range' => [
                            'start' => -100,
                            'end' => 100
                        ],
                    ],
                    'value' => 0,
                    'errors' => $errors,
                    ])
        @endcomponent
    @endcomponent
@endsection