@extends('miamilaw::layouts.miamilaw')

@section('subject')
    Edit Item
@endsection

@section('content')
    @component('miamilaw::components.form.form', [
                'action' => [
                    'route' => 'menu.items.update',
                    'parameters' => [
                        'menu_id' => $menu_id,
                        'id' => $item->id
                    ]
                ],
                'cancel' => [
                    'route' => 'menu.items.index',
                    'parameters' => [
                        'menu_id' => $menu_id
                    ]
                ],
                'method' => 'PUT'
                ])
        @component('miamilaw::components.form.hidden', [
                    'name' => 'menu_id',
                    'value' => $menu_id
                    ])
        @endcomponent
        @component('miamilaw::components.form.input', [
                    'name' => 'name',
                    'icon' => 'square',
                    'placeholder' => 'Name',
                    'required' => true,
                    'autofocus' => true,
                    'value' => $item->name,
                    'errors' => $errors,
                    ])
        @endcomponent
        @component('miamilaw::components.form.input', [
                    'name' => 'route',
                    'icon' => 'linkify',
                    'placeholder' => 'URL Route',
                    'required' => true,
                    'value' => $item->route,
                    'errors' => $errors,
                    ])
        @endcomponent
        @component('miamilaw::components.form.select', [
                    'name' => 'weight',
                    'description' => 'Select Weight',
                    'select' => [
                        'type' => 'numerical',
                        'range' => [
                            'start' => -100,
                            'end' => 100
                        ],
                    ],
                    'value' => $item->weight,
                    'errors' => $errors,
                    ])
        @endcomponent
    @endcomponent
@endsection