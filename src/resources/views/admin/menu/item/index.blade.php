@extends('miamilaw::layouts.miamilaw')

@section('subject')
    <h1 class="ui header">
        {{ $menu->description }}
    </h1>
    <h2 class="ui header">Item Configuration</h2>
@endsection

@section('content')
    @if (session()->get('success'))
        <div class="ui positive message">
            <div class="header">{{ session()->get('success') }}</div>
        </div>
    @endif
    <a href="{{ route('menu.items.create', $menu->id) }}" class="positive ui fluid large button">
        <i class="icon plus square outline"></i>
        Create new
    </a>
    <table class="ui celled striped table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Route</th>
            <th>Weight</th>
            <th>Functions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($menu->items as $item)
            <tr>
                <td data-label="Name">{{ $item->name }}</td>
                @validroute($item->route)
                    <td data-label="Route">
                        <div class="ui success message">
                            {{ $item->route }}
                        </div>
                    </td>
                @else
                    <td data-label="Route">
                        <div class="ui error message">
                            <h3 class="header">
                                <i class="yellow exclamation circle icon"></i> Invalid route
                            </h3>
                            {{ $item->route }}
                        </div>
                    </td>
                @endvalidroute
                <td data-label="Weight">{{ $item->weight }}</td>
                <td data-label="Functions">
                    <form action="{{ route('menu.items.destroy', ['menu_id' => $menu->id, 'id' => $item->id])}}" method="post" onsubmit="return confirm('Are you sure you want to Delete the Item?');">
                        @csrf
                        @method('DELETE')
                        <div class="ui left floated buttons">
                            <a href="{{ route('menu.items.edit', ['menu_id' => $menu->id, 'id' => $item->id]) }}" class="ui green button">Edit</a>
                        </div>
                        <button class="ui right floated red button" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <a href="{{ route('menu.index') }}" class="ui labeled icon teal button">
        <i class="reply icon"></i>
        Back to Menu Configuration
    </a>
@endsection