@extends('miamilaw::layouts.miamilaw')

@section('subject')
    Create new User
@endsection

@section('content')
    @component('miamilaw::components.form.form', [
                'action' => [
                    'route' => 'user.store',
                    'parameters' => [
                    ]
                ],
                'cancel' => [
                    'route' => 'user.index',
                    'parameters' => []
                ],
                ])
        @component('miamilaw::components.form.input', [
                    'name' => 'name',
                    'icon' => 'user',
                    'placeholder' => 'Full Name',
                    'required' => true,
                    'autofocus' => true,
                    'errors' => $errors,
                    ])
        @endcomponent
        @component('miamilaw::components.form.input', [
                    'name' => 'email',
                    'icon' => 'mail',
                    'placeholder' => 'Email',
                    'required' => true,
                    'errors' => $errors,
                    ])
        @endcomponent
        @authtype('local')
            @component('miamilaw::components.form.input', [
                        'name' => 'password',
                        'type' => 'password',
                        'icon' => 'key',
                        'placeholder' => 'Set Password',
                        'errors' => $errors,
                        ])
            @endcomponent
        @elseauthtype('azure')
            @component('miamilaw::components.form.input', [
                        'name' => 'azure_id',
                        'icon' => 'cloud',
                        'placeholder' => 'Azure ID',
                        'errors' => $errors,
                        ])
            @endcomponent
        @endauthtype
        @component('miamilaw::components.form.multiselect', [
                    'name' => 'roles',
                    'description' => 'Select Roles',
                    'options' => function() use ($roles){
                        $options = [];
                        foreach ($roles as $role){
                            $options[] = [
                                'value' => $role->id,
                                'name' => $role->description,
                            ];
                        }
                        return $options;
                    },
                    'errors' => $errors,
                    ])
        @endcomponent
    @endcomponent
@endsection