@extends('miamilaw::layouts.miamilaw')

@section('subject')
    {{ $user->name }}
@endsection

@section('content')
    @component('miamilaw::components.form.form', [
                'action' => [
                    'route' => 'user.update',
                    'parameters' => [
                        'user_id' => $user->id,
                    ]
                ],
                'cancel' => [
                    'route' => 'user.index',
                    'parameters' => []
                ],
                'method' => 'PUT'
                ])
        @isadmin
            @component('miamilaw::components.form.input', [
                        'name' => 'name',
                        'icon' => 'user',
                        'placeholder' => 'Full Name',
                        'required' => true,
                        'autofocus' => true,
                        'value' => $user->name,
                        'errors' => $errors,
                        ])
            @endcomponent
            @component('miamilaw::components.form.input', [
                        'name' => 'email',
                        'icon' => 'mail',
                        'placeholder' => 'Email',
                        'required' => true,
                        'value' => $user->email,
                        'errors' => $errors,
                        ])
            @endcomponent
            @authtype('local')
                @component('miamilaw::components.form.input', [
                            'name' => 'password',
                            'type' => 'password',
                            'icon' => 'key',
                            'placeholder' => 'Update Password',
                            'errors' => $errors,
                            ])
                @endcomponent
            @elseauthtype('azure')
                @component('miamilaw::components.form.input', [
                            'name' => 'azure_id',
                            'icon' => 'cloud',
                            'placeholder' => 'Azure ID',
                            'value' => $user->azure_id,
                            'errors' => $errors,
                            ])
                @endcomponent
            @endauthtype
            @component('miamilaw::components.form.multiselect', [
                        'name' => 'roles',
                        'description' => 'Select Roles',
                        'options' => function() use ($roles, $user){
                            $options = [];
                            foreach ($roles as $role){
                                $options[] = [
                                    'value' => $role->id,
                                    'name' => $role->description,
                                    'selected' => $user->hasRole($role->name)
                                ];
                            }
                            return $options;
                        },
                        'errors' => $errors,
                        ])
            @endcomponent
        @elseisadmin
            @component('miamilaw::components.form.hidden', [
                        'name' => 'name',
                        'value' => $user->name
                        ])
            @endcomponent
            @component('miamilaw::components.form.hidden', [
                        'name' => 'email',
                        'value' => $user->email
                        ])
            @endcomponent
            @authtype('local')
                @component('miamilaw::components.form.input', [
                            'name' => 'password',
                            'type' => 'password',
                            'icon' => 'key',
                            'placeholder' => 'Update Password',
                            'errors' => $errors,
                            ])
                @endcomponent
            @endauthtype
        @endisadmin
    @endcomponent
@endsection