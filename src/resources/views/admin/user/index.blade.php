@extends('miamilaw::layouts.miamilaw')

@section('subject')
    User Configuration
@endsection

@section('content')
    @if (session()->get('success'))
        <div class="ui positive message">
            <div class="header">{{ session()->get('success') }}</div>
        </div>
    @elseif (session()->get('warn'))
        <div class="ui warning message">
            <div class="header">
                {{ session()->get('warn') }}
            </div>
        </div>
    @endif
    <a href="{{ route('user.create') }}" class="positive ui fluid large button">
        <i class="icon plus square outline"></i>
        Create new
    </a>
    <table class="ui celled striped table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Roles</th>
                <th>Functions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td data-label="Id">{{ $user->id }}</td>
                    <td data-label="Name"><a href="{{route('user.show', $user->id)}}">{{ $user->name }}</a></td>
                    <td data-label="Email">{{ $user->email }}</td>
                    <td data-label="Roles">
                        @forelse ($user->roles as $role)
                            {{ $role->name }}@if(!$loop->last), @endif
                        @empty
                            No roles assigned
                        @endforelse
                    </td>
                    <td data-label="Functions">
                        <form action="{{ route('user.destroy', $user)}}" method="post" onsubmit="return confirm('Are you sure you want to Delete the User?');">
                            @csrf
                            @method('DELETE')
                            <div class="ui left floated buttons">
                                <a href="{{ route('user.edit',$user->id) }}" class="ui green button">Edit</a>
                            </div>
                            <button class="ui right floated red button" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection