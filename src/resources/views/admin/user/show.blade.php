@extends('miamilaw::layouts.miamilaw')

@section('subject')
    User Profile
@endsection

@section('content')
    @if (session()->get('success'))
        <div class="ui positive message">
            <div class="header">{{ session()->get('success') }}</div>
        </div>
    @endif
    <div class="ui centered fluid card">
        <div class="content">
            <img class="right floated ui avatar image" src="{{asset('vendor/miamilaw/img/profile.jpg')}}">
            <div class="header">{{ $user->name }}</div>
            <div class="meta">
                @foreach($user->roles as $role)
                    <span>{{ $role->name }}</span>@if(!$loop->last), @endif
                @endforeach
            </div>
        </div>
        <div class="content">
            <div class="ui feed">
                <div class="event">
                    <div class="label">
                        <i class="mail icon"></i>
                    </div>
                    <div class="content">
                        <div class="summary">
                            Email
                        </div>
                        <div class="extra text">
                            {{ $user->email }}
                        </div>
                    </div>
                </div>
                @authtype('local')
                    <div class="event">
                        <div class="label">
                            <i class="key icon"></i>
                        </div>
                        <div class="content">
                            <div class="summary">
                                Password
                            </div>
                            <div class="extra text">
                                @if($user->password)
                                    Is set
                                @else
                                    Not set
                                @endif
                            </div>
                        </div>
                    </div>
                @elseauthtype('azure')
                    <div class="event">
                        <div class="label">
                            <i class="cloud icon"></i>
                        </div>
                        <div class="content">
                            <div class="summary">
                                Azure ID
                            </div>
                            <div class="extra text">
                                {{ $user->azure_id }}
                            </div>
                        </div>
                    </div>
                @endauthtype
                <div class="event">
                    <div class="label">
                        <i class="clock icon"></i>
                    </div>
                    <div class="content">
                        <div class="summary">
                            Created On
                        </div>
                        <div class="extra text">
                            {{ $user->created_at }}
                        </div>
                        <div class="meta">
                            <i class="edit icon"></i> Last modified on <div class="date">{{ $user->updated_at }}</div>
                        </div>
                    </div>
                </div>
                <div class="event">
                    <div class="label">
                        <i class="clock icon"></i>
                    </div>
                    <div class="content">
                        <div class="summary">
                            Last login
                        </div>
                        <div class="extra text">
                            {{ $user->last_login_at }} from {{ $user->last_login_ip }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="extra content">
            <span>
                <i class="user icon"></i>
                @foreach($user->roles as $role)
                    <span>{{ $role->description }}</span>@if(!$loop->last), @endif
                @endforeach
            </span>
        </div>
        <a href="{{ route('user.edit', $user->id) }}" class="ui bottom attached button">
            <i class="edit icon"></i>
            Edit
        </a>
    </div>
@endsection