@extends('miamilaw::layouts.miamilaw')

@section('content')
<div class="container">
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui green image header">
                <!--<img src="{{ asset('vendor/miamilaw/img/footer-logo.png') }}" class="image">-->
                <div class="content">
                    {{ __('Login') }}
                </div>
            </h2>
            <form class="ui large form" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="ui stacked segment">
                    <div class="field">
                        <div class="ui left icon input @error('email') error @enderror">
                            <i class="user icon"></i>
                            <input id="email" type="email" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <div class="ui error message">
                                <strong>{{ $message }}</strong>
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input @error('password') error @enderror">
                            <i class="lock icon"></i>
                            <input id="password" type="password" placeholder="{{ __('Password') }}" name="password" required autocomplete="current-password">

                            @error('password')
                            <div class="ui error message">
                                <strong>{{ $message }}</strong>
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large green submit button">{{ __('Login') }}</button>
                </div>

                <div class="ui error message"></div>

            </form>


            @if (Route::has('password.request'))
            <div class="ui message">
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
