<div class="field">
    <div class="ui checkbox
                @isset($disabled) @if($disabled) disabled @endif @endisset
                @error($name) error @enderror">
        <input type="checkbox"
               name="{{ $name }}"
               id="{{ $name }}"
               @isset($checked) @if($checked) checked @endif @endisset
               @isset($disabled) @if($disabled) disabled @endif @endisset>
        <label for="{{ $name }}">
            {{ $description }}
        </label>

        @error($name)
        <div class="ui error message">
            <strong>{{ $message }}</strong>
        </div>
        @enderror
    </div>
</div>