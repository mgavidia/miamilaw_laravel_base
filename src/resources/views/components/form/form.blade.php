<form @isset($name) id="{{$name}}" @endisset class="ui form" method="POST" action="{{ route($action['route'], $action['parameters']) }}">
    <div class="ui stacked segment">
        @csrf
        @isset($method)
            @method($method)
        @endisset
        {{ $slot }}
        @isset($cancel)
            @component('miamilaw::components.form.submit', [
                        'cancel' => $cancel
                        ])
            @endcomponent
        @else
            @component('miamilaw::components.form.submit')
            @endcomponent
        @endisset
    </div>
    <div class="ui error message">
    </div>
</form>
<!-- Validator -->
<script>
    @isset($name)
        $.fn.form.settings.rules.startsWith = function(value, text) {
          return value.substring(0, text.length) == text
        };
        $.fn.form.settings.rules.notStartsWith = function(value, text) {
          return value.substring(0, text.length) != text
        };
        $('#{{ $name }}')
            .form({
              on: 'blur',
              fields: {
                @isset($validators)
                    @foreach($validators as $key => $rule)
                        @if(is_array($rule))
                            {{ $key }}: @php echo json_encode($rule, JSON_PRETTY_PRINT) @endphp
                        @elseif(is_string($rule))
                            {{ $key }}: @php echo $rule @endphp @if(!$loop->last) , @endif
                        @endif
                    @endforeach
                @endisset
              }
            });
    @endisset
</script>