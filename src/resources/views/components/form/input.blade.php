<div class="field">
    <div class="ui left icon input
                @isset($disabled) @if($disabled) disabled @endif @endisset
                @isset($name) {{ $errors->has($name) ? 'error' : '' }} @endisset">
        <i class="{{ $icon }} icon"></i>
        <input
               @isset($name) id="{{ $name }}" @endisset
               @isset($name) name="{{ $name }}" @endisset
               @isset($type) type="{{ $type }}" @endisset
               @isset($placeholder) placeholder="{{ $placeholder }}" @endisset
               @isset($value) value="{{ $value }}" @endisset
               @isset($required) @if($required) required @endif @endisset
               @isset($autofocus) @if($autofocus) autofocus @endif @endisset
               @isset($disabled) @if($disabled) disabled @endif @endisset>
        @isset($name)
            @error($name)
            <div class="ui error message">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        @endisset
    </div>
</div>