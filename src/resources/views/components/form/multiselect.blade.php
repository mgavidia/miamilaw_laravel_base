<div class="field">
    <select name="{{ $name }}[]" id="{{ $name }}_multiselect" multiple="" class="ui fluid normal dropdown @error($name) error @enderror">
        <option value="">{{ $description }}</option>
        @if(is_callable($options))
            @foreach($options() as $option)
                <option
                    value="{{ $option['value'] }}"
                    @isset($option['selected']) @if($option['selected']) selected @endif @endisset
                >
                    {{ $option['name'] }}
                </option>
            @endforeach
        @else
            @foreach($options as $option)
                <option
                    value="{{ $option['value'] }}"
                    @isset($option['selected']) @if($option['selected']) selected @endif @endisset
                >
                    {{ $option['name'] }}
                </option>
            @endforeach
        @endif
    </select>
    @error($name)
    <div class="ui error message">
        <strong>{{ $message }}</strong>
    </div>
    @enderror
    <script>
      $('#{{ $name }}_multiselect')
        .dropdown()
      ;
    </script>
</div>