<div class="field">
    <div id="{{ $name }}_selection" class="ui selection dropdown @error($name) error @enderror">
        <i class="dropdown icon"></i>
        <input id="route" type="hidden" name="{{ $name }}">
        <div class="default text">{{ $description }}</div>
        <div class="menu">
            @if ($select['type'] === 'numerical')
                @for($i = $select['range']['start']; $i <= $select['range']['end']; $i++)
                    <div class="item" data-value="{{ $i }}">{{ $i }}</div>
                @endfor
            @elseif ($select['type'] === 'array')
                @foreach($select['items'] as $item)
                    <div class="item" data-value="{{ $item['value'] }}">{{ $item['name'] }}</div>
                @endforeach
            @endif
        </div>
        @error($name)
        <div class="ui error message">
            <strong>{{ $message }}</strong>
        </div>
        @enderror
    </div>
    <script>
      $('#{{ $name }}_selection')
        .dropdown(
          @isset($value) 'set selected', '{{ $value }}' @endisset
        )
      ;
    </script>
</div>