<div class="ui buttons">
    @isset($cancel)
        <a href="{{ route($cancel['route'], $cancel['parameters']) }}" class="ui fluid large red button">Cancel</a>
        <div class="or"></div>
    @endisset
    <button type="submit" class="ui fluid large green submit button">Save</button>
</div>