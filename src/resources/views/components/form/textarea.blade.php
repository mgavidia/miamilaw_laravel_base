<div class="field">
    <style>
        .field.error > #cke_{{ $name }}-editor {
            border: 2px solid #e0b4b4;
        }
    </style>
    <textarea
            name="{{$name}}"
            id="{{$name}}-editor"
            @isset($autofocus) autofocus @endisset
            @isset($placeholder) placeholder="{{ $placeholder }}" @endisset
            @isset($readonly) readonly @endisset
            @isset($required) required @endisset
            @isset($disabled) disabled @endisset>
        @isset($value){{ $value }}@endisset
    </textarea>
    @error($name)
    <div class="ui error message">
        <strong>{{ $message }}</strong>
    </div>
    @enderror
    <script>
      CKEDITOR.replace('{{$name}}-editor', {
        contentsCss: 'https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css',
        removeButtons: 'Save,Flash',
      })
    </script>
</div>
