@extends('miamilaw::layouts.miamilaw')

@section('content')
<div class="ui container">
    <div class="ui segment">
        <div class="ui header">
            Dashboard
        </div>
        <div class="description">
            @if (session('status'))
                <div class="ui message" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            You are logged in!
        </div>
    </div>
</div>
@endsection
