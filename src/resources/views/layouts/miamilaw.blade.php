<?php
/**
 * Created by PhpStorm.
 * User: mgavidia
 * Date: 5/16/19
 * Time: 10:00 AM
 */
?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>University of Miami School of Law - @yield('title')</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('vendor/miamilaw/img/favicon.ico') }}">

        <!-- Fonts -->

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
        <link rel="stylesheet" href="{{ asset('vendor/miamilaw/css/miamilaw-base.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('vendor/miamilaw/js/miamilaw-base.js') }}"></script>
        <script src="https://cdn.ckeditor.com/4.11.4/full-all/ckeditor.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js"></script>
    </head>
    <body>
        <div class="ml-body">
            <!-- Main menu bar -->
            @isadmin
            <div class="ui inverted menu ml-utility-menu">
                <div class="header item">
                    Administration Menu
                </div>
                @rendermenu(admin-menu)
                <div class="right menu">
                    <div class="ui simple dropdown item">
                        {{ Auth::User()->name }}
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="disabled item">{{ Auth::User()->email }}</div>
                            <a href="{{ route('user.profile') }}" class="item">Profile</a>
                            @rendermenu(user-menu)
                            <a href="{{ url('/logout') }}" class="item">Logout</a>
                        </div>
                    </div>
                </div>
            </div>
            @endisadmin
            <!-- Header -->
            <div class="ml-header">
                <a href="/">
                    <img class="ui large image" src="{{ asset('vendor/miamilaw/img/logo.png') }}">
                </a>
            </div>

            <div class="ui inverted menu ml-utility-menu">
                @rendermenu(main-menu)
                @notadmin
                    @if (Route::has('login'))
                        <div class="right menu">
                            @auth
                                <div class="ui simple dropdown item">
                                    {{ Auth::User()->name }}
                                    <i class="dropdown icon"></i>
                                    <div class="menu">
                                        <div class="disabled item">{{ Auth::User()->email }}</div>
                                        <a href="{{ route('user.profile') }}" class="item">Profile</a>
                                        @rendermenu(user-menu)
                                        <a href="{{ url('/logout') }}" class="item">Logout</a>
                                    </div>
                                </div>
                            @else
                                <a href="{{ url('/login') }}" class="ui item">
                                    Login
                                </a>
                            @endauth
                        </div>
                    @endif
                @endnotadmin
            </div>
            <!-- Content -->
            <div class="ml-content">
                <div class="ui container">
                    <h1 class="ui header">@yield('subject')</h1>
                    @yield('content')
                </div>
            </div>
            <!-- Footer -->
            <div class="ml-footer">
                <div class="ui grid">
                    <div class="sixteen wide mobile four wide tablet four wide computer column logo">
                        <a href="/">
                            <img src="{{ asset('vendor/miamilaw/img/footer-logo.png') }}" class="ui medium image">
                        </a>

                        <p>
                            1311 Miller Drive, Coral Gables, FL 33146<br>
                            305-284-2339<br>
                            <br>
                            &#169; <?php echo date('Y'); ?> University of Miami<br>
                            All Rights Reserved
                            <br>
                            <span class="miamilaw-time"></span>
                        </p>
                    </div>
                    <!--
                    <div class="sixteen wide mobile twelve wide tablet seven wide computer column links">
                        <div class="ui centered grid">
                            <div
                                 class="sixteen wide mobile eight wide tablet eight wide computer column first">
                                <div class="ui list section">
                                    <div class="item big">Prospective Students</div>
                                    <a href="/admissions" class="item">Fast Facts</a>
                                    <a href="/students" class="item">Areas of Focus</a>
                                    <a href="/students" class="item">Practical Experience</a>
                                    <a href="/students" class="item">Request Information</a>
                                    <a href="/students" class="item">Apply</a></div>
                                <div class="ui list section">
                                    <div class="item big">Legal</div>
                                    <a href="/students" class="item">ABA Required Disclosures</a><a
                                            href="/students" class="item">Legal Notices</a><a
                                            href="/students" class="item">Emergency Information</a><a
                                            href="/students" class="item">Rules &amp; Policies</a></div>
                            </div>
                            <div
                                 class="sixteen wide mobile eight wide tablet eight wide computer column second">
                                <div class="ui list section">
                                    <div class="item big">Quicklinks</div>
                                    <a href="/directory" class="item">Directory</a><a
                                                                                                         href="/iml/registrar"
                                                                                                         class="item">Registrar</a><a
                                            href="/directory" class="item">Calendars</a><a
                                            href="https://mail.miami.edu" class="item">Office 365
                                        Email</a><a href="https://canelink.miami.edu" class="item">Canelink</a>
                                </div>
                                <div class="ui list section">
                                    <div class="item big">External Affairs</div>
                                    <a href="/students" class="item">About Us</a><a
                                                                                                       href="/faculty"
                                                                                                       class="item">Faculty
                                        Experts</a><a href="/news" class="item">News Stories</a><a
                                            href="/students" class="item">Contact Us</a></div>
                            </div>
                        </div>
                    </div>
                    <div
                         class="sixteen wide mobile sixteen wide tablet five wide computer centered column social">
                        <div class="ui tiny images"><a
                                                                          href="https://www.facebook.com/MiamiLawSchool/"><img

                                        src="http://hadoop-02.law.miami.edu/sites/default/files/menu_icons/menu_icon_1540.gif"
                                        class="ui centered circular image"></a><a><img
                                                                                                          src="http://hadoop-02.law.miami.edu/sites/default/files/menu_icons/menu_icon_1602.gif"
                                                                                                          class="ui centered circular image"></a><a
                                   ><img
                                                            src="http://hadoop-02.law.miami.edu/sites/default/files/menu_icons/menu_icon_1539.gif"
                                                            class="ui centered circular image"></a><a
                                   ><img
                                                            src="http://hadoop-02.law.miami.edu/sites/default/files/menu_icons/menu_icon_1541.gif"
                                                            class="ui centered circular image"></a><a
                                   ><img
                                                            src="http://hadoop-02.law.miami.edu/sites/default/files/menu_icons/menu_icon_1605.gif"
                                                            class="ui centered circular image"></a><a
                                   ><img
                                                            src="http://hadoop-02.law.miami.edu/sites/default/files/menu_icons/menu_icon_1604.gif"
                                                            class="ui centered circular image"></a><a
                                   ><img
                                                            src="http://hadoop-02.law.miami.edu/sites/default/files/menu_icons/menu_icon_1603.gif"
                                                            class="ui centered circular image"></a></div>
                    </div>
                    -->
                </div>
            </div>
        </div>
</body>
</html>
