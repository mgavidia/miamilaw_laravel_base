<?php

// Auth Configuration
if (config('miamilaw.auth') === 'azure') {
    Route::get('/login', function () {
        return redirect('/login/microsoft');
    })->name('login');

    Route::match(['GET', 'POST'], '/logout', function () {
        Auth::logout();
        Session::flush();

        return redirect('/');
    })->name('logout');
} elseif (config('miamilaw.auth') === 'local') {
    Route::get('/login/microsoft', function () {
        return redirect('/login');
    });

    Route::get('/login/microsoft/callback', function () {
        return redirect('/login');
    });

    Route::group(['middleware' => ['web'], 'namespace'=>'App\Http\Controllers'], function () {
        Auth::routes(['register' => false, 'reset' => false, 'verify' => false]);

        Route::get('/logout', function () {
            Auth::logout();
            Session::flush();

            return redirect('/');
        });
    });
}

// MiamiLaw Controllers
Route::group(['namespace' => '\MiamiLaw\Base\Http\Controllers'], function () {

    // User Controller
    Route::resource('admin/user', 'UserController');
    Route::get('/user/profile', 'UserController@profile')
        ->name('user.profile');

    // Menu Admin
    Route::resource('admin/menu', 'MenuController')->except(['show']);
    Route::get('admin/menu/{menu}/items', 'MenuController@itemsIndex')
         ->name('menu.items.index');
    Route::get('admin/menu/{menu}/items/create', 'MenuController@itemsCreate')
         ->name('menu.items.create');
    Route::post('admin/menu/{menu}/items', 'MenuController@itemsStore')
         ->name('menu.items.store');
    Route::get('admin/menu/{menu}/items/{item}/edit', 'MenuController@itemsEdit')
         ->name('menu.items.edit');
    Route::delete('admin/menu/{menu}/items/{item}', 'MenuController@itemsDestroy')
         ->name('menu.items.destroy');
    Route::match(['put', 'patch'], 'admin/menu/{menu}/items/{item}', 'MenuController@itemsUpdate')
         ->name('menu.items.update');
});
