const mix = require('laravel-mix');

// mix.config.fileLoaderDirs.fonts = 'vendor/miamilaw/fonts';

mix.webpackConfig({
  module: {
    rules: [
    ]
  }
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('src/resources/js/miamilaw-base.js', 'src/resources/dist/js')
    .sass('src/resources/sass/miamilaw-base.scss', 'src/resources/dist/css');